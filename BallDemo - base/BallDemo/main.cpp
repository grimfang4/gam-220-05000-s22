#include <iostream>
#include <vector>
#include "SDL_gpu.h"
#include "NFont_gpu.h"
using namespace std;


void UpdateBall(GPU_Target* target, float dt, float& ballX, float& ballY, float& ballVelocityX, float& ballVelocityY)
{
	// Forward Euler integration: Add a little slice of our velocity to our position each frame.
	// This makes things appear to move!
	ballX += ballVelocityX * dt;
	ballY += ballVelocityY * dt;
}

void DrawBall(GPU_Target* target, float ballX, float ballY)
{
	GPU_CircleFilled(target, ballX, ballY, 50, GPU_MakeColor(200, 50, 50, 255));
}

struct Vector3
{
	float x, y, z;
};

class Enemy
{
public:
	float hitpoints;
	float power;
	float shields;
};

int main(int argc, char* argv[])
{
	GPU_Target* screen = GPU_Init(800, 600, GPU_DEFAULT_INIT_FLAGS);
	if (screen == nullptr)
		return 1;

	SDL_SetWindowTitle(SDL_GetWindowFromID(screen->context->windowID), "Ball Demo");
	

	// Store values to represent the physical position and motion of a ball in the middle of the screen
	float ballX = screen->w / 2;
	float ballY = screen->h / 2;
	float ballVelocityX = 20;
	float ballVelocityY = 40;


	// if(myVector3 == otherVector3)
	//    ...

	// if(enemy1 == enemy2)
	//    ...



	// Set up variables for the main game loop
	NFont font;
	font.load("FreeSans.ttf", 14);

	const Uint8* keystates = SDL_GetKeyboardState(nullptr);

	int mx = 0, my = 0;

    // Delta time, the time it took to complete the last frame.
	// This is recalculated every frame.
	float dt = 0.0f;
	Uint32 startTime = SDL_GetTicks();
	Uint32 endTime = 0;


	float myTimer = 5.0f;
	bool myTimerExpired = false;


	SDL_Event event;
	bool done = false;
	while (!done)  // Main game loop
	{


		// SDL_PollEvent() will get one user input event from SDL's event queue
		while (SDL_PollEvent(&event))  // Event loop, process all user input events that occurred
		{
			if (event.type == SDL_QUIT)
				done = true;
			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
					done = true;
			}
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				if (event.button.button == SDL_BUTTON_LEFT)
				{
					GPU_Log("Mouse clicked at: (%d, %d)\n", event.button.x, event.button.y);
				}
				if (event.button.button == SDL_BUTTON_RIGHT)
				{

				}
			}
		}

		SDL_GetMouseState(&mx, &my);

		UpdateBall(screen, dt, ballX, ballY, ballVelocityX, ballVelocityY);

		if (!myTimerExpired)
		{
			myTimer -= dt;
			if (myTimer <= 0.0f)
			{
				// Timer expired!
				GPU_Log("Timer expired!\n");
				//myTimer = 5.0f;
				myTimerExpired = true;
				myTimer = 0.0f;
			}
		}



		GPU_ClearRGB(screen, 255, 255, 255);

		DrawBall(screen, ballX, ballY);

		font.draw(screen, screen->w - 50, 10, NFont::AlignEnum::RIGHT, 
			"Mouse at: (%d, %d)", mx, my);

		if(myTimer > 1.0f)
			font.draw(screen, 50, 10, "Time left: %.1f", myTimer);
		else
			font.draw(screen, 50, 10, NFont::Effect(NFont::AlignEnum::LEFT, NFont::Color(255, 0, 0)), "Time left: %.1f", myTimer);

		// Update the monitor to show all of our changes
		GPU_Flip(screen);

		// Give the OS a bit of time to schedule other things
		SDL_Delay(1);

		// Measure how long the frame took
		endTime = SDL_GetTicks();
		dt = (endTime - startTime) / 1000.0f;
		startTime = endTime;
	}

	// Clean up our allocated objects before the program ends
	font.free();

	GPU_Quit();

	return 0;
}