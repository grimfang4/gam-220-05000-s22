#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <map>
using namespace std;

/* C#
Vector3 a = new Vector3();
a.x = 5;
a.Normalize();

Vector3.Dot(a, b);  // Dot product
a = Vector3.zero;  // Static value of a "zero" vector
*/

/* C++
Vector3 a;
a.x = 5;
a.Normalize();

Vector3::Dot(a, b);
a = Vector3::zero;

*/

class A
{
public:
	// How to make a variable that lives in the "class scope"
	static int zero;
};

int main(int argc, char* argv[])
{
	// Vectors are extensible arrays
	// [0, 1, 2, 3, 4]
	vector<string> names;
	names.push_back("Alice");
	names.push_back("Henry");
	names.push_back("Bob");
	names.push_back("Pauline");
	names.push_back("Charlie");

	for (int i = 0; i < names.size(); ++i)
	{
		cout << names[i] << " ";
	}
	cout << endl;

	//names.erase(names.begin() + 2);
	names.insert(names.begin() + 2, "New name");

	// Linked lists are a chain of connected values
	// [0] -> [1] -> [2] -> [3] -> [4]
	list<string> listNames;
	listNames.push_back("Alice");
	listNames.push_back("Henry");
	listNames.push_back("Bob");
	listNames.push_back("Pauline");
	listNames.push_back("Charlie");
	listNames.push_front("Other Charlie");

	for (list<string>::iterator e = listNames.begin(); e != listNames.end(); ++e)
	{
		cout << *e << " ";
	}
	cout << endl;

	// Sets are like mathematical sets: unique classifications of values
	set<int> myNumbers;
	myNumbers.insert(6);
	myNumbers.insert(3);
	myNumbers.insert(8);
	myNumbers.insert(19);
	myNumbers.insert(1);

	for (set<int>::iterator e = myNumbers.begin(); e != myNumbers.end(); ++e)
	{
		cout << *e << " ";
	}
	cout << endl;

	set<int>::iterator foundNumber = myNumbers.find(8);
	if (foundNumber == myNumbers.end())
	{
		cout << "Number not found in set!" << endl;
	}
	else
	{
		cout << "Found it in set!" << endl;
	}

	foundNumber = myNumbers.find(8888);
	if (foundNumber == myNumbers.end())
	{
		cout << "Number not found in set!" << endl;
	}
	else
	{
		cout << "Found it in set!" << endl;
	}


	map<string, string> aDictionary;
	// C#: aDictionary.Add("Key", "Value");
	aDictionary.insert(make_pair("key", "A thing that unlocks a door."));
	aDictionary.insert(make_pair("chair", "A place to sit."));

	cout << aDictionary["key"] << endl;
	cout << aDictionary["chair"] << endl;
	cout << aDictionary["A thing that unlocks a door."] << endl;

	aDictionary["bed"] = "A thing you sleep on.";

	if (aDictionary.find("ad lashdl kj") == aDictionary.end())
	{
		cout << "Value not found in dictionary." << endl;
	}
	else
	{
		cout << "That was in there?!" << endl;
	}

	cout << "Printing all mappings..." << endl;
	for (map<string, string>::iterator e = aDictionary.begin(); e != aDictionary.end(); ++e)
	{
		pair<string, string> p = *e;
		cout << p.first << ": " << p.second << endl;
	}

	auto i = 5;  // Let the compiler deduce the type instead of me saying it.

	//for (vector<string>::iterator e = names.begin(); e != names.end(); ++e)
	for (auto e = names.begin(); e != names.end(); ++e)
	{
		cout << *e << " ";
	}
	cout << endl;

	for (auto& s : names)  // range-based 'for' loop
	{
		cout << s << " ";
	}
	cout << endl;


	return 0;
}