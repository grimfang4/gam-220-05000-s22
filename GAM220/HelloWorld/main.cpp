/*using System;

class Program
{
	static void Main(string[] args)
	{
		Console.WriteLine("Hello world!");
		Console.ReadLine();
	}
}*/

#include <iostream>  // Bring in some code for input and output
using namespace std;  // Avoid having to say "std" every time I use an object from that namespace

void MyQuitMessage()
{
	cout << "OH NO!!" << endl;
	cin.get();
	//cin.ignore(9999, '\n');
}

int main(int argc, char* argv[])
{
	int n = 6;
	cout << "Hello world!" << n << 6543 << endl;  // Print a message out to the console
	cout << n << endl << "FSJFAKJLDKKL";  // endl (end line) can be put in anywhere
	cout << "asffdsa";  // More text on the same line
	cout << "Some\nmore\ntext\n";

	cin.get();  // Makes us wait until the user presses enter (but!!!  details...)

	return 0;
}