#include <iostream>
#include <vector>
using namespace std;

class Vehicle
{
public:
	int numSeats;
	float topSpeed;

	virtual void Steer(float angle) = 0;  // pure virtual function
	// Does not have an implementation, must be overridden by child classes
	// or else the child class is also an abstract class.
	virtual void Accelerate(float power) = 0;
	virtual void Brake(float power) = 0;

};

class WheeledVehicle : public Vehicle
{
protected:
	float speed;
	float direction;

public:

	WheeledVehicle()
	{
		speed = 0.0f;
		direction = 0.0f;
	}

	virtual void Steer(float angle) override
	{
		direction += angle;
	}
	virtual void Accelerate(float power) override
	{
		speed += power;
		cout << "Wheeled vehicle accelerate!" << endl;
	}
	virtual void Brake(float power) override
	{
		if (speed > 0)
		{
			speed -= power;
			if (speed < 0)
				speed = 0;
		}
		else if (speed < 0)
		{
			speed += power;
			if (speed > 0)
				speed = 0;
		}
	}
};

class Racecar : public WheeledVehicle
{
public:
	int numOfStickers;

};

class Tractor : public WheeledVehicle
{
public:
	float dragCapacity;

	void LiftFrontScoop()
	{
		// TODO
	}

	virtual void Accelerate(float power) override
	{
		// Do tractory things...
		cout << "Tractor accelerate!" << endl;
	}
};

class Airplane : public Vehicle
{
protected:
	float speed;
	float direction;

public:

	Airplane()
	{
		speed = 0.0f;
		direction = 0.0f;
	}

	virtual void Steer(float angle) override
	{
		direction += angle;
	}
	virtual void Accelerate(float power) override
	{
		speed += power;
	}
	virtual void Brake(float power) override
	{
		if (speed > 0)
		{
			speed -= power;
			if (speed < 0)
				speed = 0;
		}
		else if (speed < 0)
		{
			speed += power;
			if (speed > 0)
				speed = 0;
		}
	}
};

int main(int argc, char* argv[])
{
	Tractor myVehicle;

	myVehicle.Accelerate(100);

	Airplane myPlane;
	Racecar myCar;

	Vehicle* someVehicle;
	someVehicle = &myVehicle;
	someVehicle->Brake(10);

	someVehicle = &myPlane;
	someVehicle->Steer(25.0f);

	vector<Vehicle*> vehicles;
	vehicles.push_back(&myVehicle);
	vehicles.push_back(&myPlane);
	vehicles.push_back(new Racecar());

	for (auto v : vehicles)
	{
		v->Accelerate(15);
	}


	return 0;
}