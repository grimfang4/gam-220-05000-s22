#include <iostream>
#include <string>
using namespace std;

string GetOptions(char digit)
{
	switch (digit)
	{
	case '2':
		return "abc";
	case '3':
		return "def";
	case '4':
		return "ghi";
	case '5':
		return "jkl";
	case '6':
		return "mno";
	case '7':
		return "pqrs";
	case '8':
		return "tuv";
	case '9':
		return "wxyz";
	default:
		return string(1, digit);
	}
}

void BuildString(string seed, string input, int i)
{
	if (i >= input.size())
	{
		cout << seed << endl;
		return;
	}

	string o = GetOptions(input[i]);
	for (int j = 0; j < o.size(); ++j)
	{
		BuildString(seed + string(1, o[j]), input, i + 1);
	}
}

int main(int argc, char* argv[])
{
	string input = "2676";

	string o = GetOptions(input[0]);
	for (int j = 0; j < o.size(); ++j)
	{
		BuildString(string(1, o[j]), input, 1);
	}

	return 0;
}