#include <iostream>
using namespace std;

void Sort(int* start, int* end)
{

}

void Sort(int* array, int startIndex, int endIndex)
{

}

int main(int argc, char* argv[])
{
	int a = 5;

	int* myPtr = &a;

	cout << myPtr << endl;

	// Write access violation
	//myPtr = (int*)678987654;
	//*myPtr = 5678;

	// Pointer arithmetic
	int array[] = {0, 1, 2, 3, 4, 5};

	int index = 0;
	for (myPtr = array; index < 5; ++myPtr)
	{
		cout << *myPtr;
		++index;
	}

	const char* myString = "Hello!";

	for (const char* c = myString; *c != '\0'; ++c)
	{
		cout << *c;
	}
	cout << endl;

	for (int i = 0; i < 6; ++i)
	{
		cout << myString[i];
	}
	cout << endl;

	const char* c = myString + 3;
	cout << endl << *c << endl;

	c += 2;
	c -= 3;

	int count = 0;
	for (const char* c = myString; *c != '\0'; c++)
	{
		++count;
	}
	cout << count << endl;

	// Print out every 4th letter of the string
	for (const char* c = myString; *c != '\0'; c += 4)
	{
		cout << *c << endl;
	}

	cin.get();


	return 0;
}