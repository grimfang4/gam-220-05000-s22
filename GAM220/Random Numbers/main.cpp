#include <iostream>
#include <cstdlib>
#include <ctime>
#include <random>
#include "Random.h"
using namespace std;

void DoRandomThing(Random& rng)
{
	cout << rng.Integer(0, 100) << endl;
}


int main(int argc, char* argv[])
{
	// Deterministic vs. Nondeterministic

	srand(time(NULL));
	int randomNumber = rand();
	// rand goes from 0 to 32767
	cout << randomNumber << endl;

	// Smaller value?
	randomNumber = rand()%11; // 0 - 10
	cout << randomNumber << endl;


	std::mt19937 generator;
	generator.seed(time(NULL));
	uniform_int_distribution<int> intDist(-5, 50);
	randomNumber = intDist(generator);

	uniform_real_distribution<float> floatDist(0.0f, 1.0f);
	float randFloat = floatDist(generator);


	Random rng;




	randomNumber = rng.Integer(0, 11);
	randFloat = rng.Float(0.0f, 1.0f);

	DoRandomThing(rng);

	return 0;
}