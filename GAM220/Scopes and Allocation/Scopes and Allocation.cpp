#include <iostream>
using namespace std;

void MyOtherFunction()
{
    int more;
}

void MyFunction()
{
    int myVar = 5;
    myVar *= 6;

    cout << myVar << endl;

    MyOtherFunction();

    myVar = 89;
}

void AnotherFunction(int* b)
{
    // Do other things with b
}

int main(int argc, char* argv[])
{
    cout << "Hello World!" << endl;

    MyFunction();

    for (int i = 0; i < 10; ++i)
    {
        int a = 5;  // stack-allocated
    }

    // heap-allocated
    int* b = new int();

    // Do things with b
    AnotherFunction(b);

    delete b;

    return 0;
}