#include <iostream>
#include <string>
using namespace std;

int main(int argc, char* argv[])
{
	int myInt;
	int myArray[4];
	int myOtherArray[4];

	myArray[0] = 43;
	myArray[1] = 3939;
	myArray[2] = 24;
	myArray[3] = 279;

	for (int i = 0; i < 4; ++i)
	{
		myArray[i] = i*2;
	}

	char myCharacters[] = {'a', 'b', 'c', '\0'};
	// How similar are strings and arrays of characters?

	cout << myCharacters << endl;

	cout << "Hello!" << endl;
	cout << myArray << endl;
	cout << myOtherArray << endl;

	cout << sizeof(int) << endl;

	string myString = "Some string";
	cout << "String size: " << myString.size() << endl;

	const char* stringRep = myString.c_str();  // Give me the character array underlying this string object
	// char* is a POINTER to a character


	cout << (int)myArray << endl;
	cout << (int)myOtherArray << endl;
	cout << (int)stringRep << endl;


	// new operator: Allocate some memory for a new object
	int* heapArray = new int[5];
	int heapArraySize = 5;

	for (int i = 0; i < heapArraySize; ++i)
	{
		heapArray[i] = i;  // Pointers can be used like arrays
	}

	delete[] heapArray;


	string words[] = {"Apple", "Bacon", "Cheddar"};

	/*char currentLetter = 'a';
	for (int i = 0; i < 3; ++i)
	{
		cout << currentLetter++ << " is for " << words[i] << endl;
	}*/

	/*for (int i = 0; i < 3; ++i)
	{
		cout << char('a' + i) << " is for " << words[i] << endl;
	}*/

	for (char c = 'a'; c <= 'c'; ++c)
	{
		cout << c << " is for " << words[c - 97] << endl;
	}



	cin.get();

	return 0;
}