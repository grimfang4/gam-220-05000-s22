#include <iostream>
#include <vector>
using namespace std;

class MyClass
{
public:
	MyClass()
	{
		id = 4;
	}
	MyClass(int newID)
	{
		id = newID;
	}
	int GetID()
	{
		return id;
	}
private:
	int id;
};

int main(int argc, char* argv[])
{
	vector<MyClass*> objects;
	for (int i = 0; i < 10; ++i)
	{
		MyClass* newObj = new MyClass(i+1);
		objects.push_back(newObj);
	}

	// Careful!  Haven't called delete on that object, so this leaks.
	//objects.erase(objects.begin() + 4);

	for (int i = 0; i < objects.size(); )
	{
		if (objects[i]->GetID() > 3 && objects[i]->GetID() < 6)
		{
			cout << "Deleting object with id: " << objects[i]->GetID() << endl;
			delete objects[i];
			objects.erase(objects.begin() + i);
		}
		else
			++i;
	}






	for (int i = 0; i < objects.size(); ++i)
	{
		delete objects[i];
	}
	objects.clear();

	return 0;
}