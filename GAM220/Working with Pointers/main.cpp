#include <iostream>
using namespace std;

// Calculates the number of bytes between these two memory addresses
// Note: sizeof(int) tells you how many bytes are in an integer
int BytesBetween(int* start, int* end)
{

}

// Prints to the console the elements of the array
// Remember not to use array indexing for this!
void Print(int* array, int size)
{

}

// Prints to the console the elements of the array, but in reverse order (i.e. starting from the end)
// Remember not to use array indexing for this!
void PrintReversed(int* array, int size)
{

}

// Returns the number of characters in the given string.
int GetStringLength(const char* someString)
{

}

int main(int argc, char* argv[])
{
	int numbers[5] = {4, 7, 3, 8, 10};

	cout << BytesBetween(&numbers[1], &numbers[3]) << endl;
	cout << "^ Should not be " << (numbers[3] - numbers[1]) << " or " << (3 - 1) << endl;
	cout << endl;

	Print(numbers, 5);
	cout << endl;
	PrintReversed(numbers, 5);
	cout << endl;

	if (GetStringLength("Hello!") == 6 && GetStringLength("") == 0 && GetStringLength("A longer string...") == 18)
	{
		cout << "Got the right string lengths." << endl;
		return 0;
	}
	else
	{
		cout << "That is not the right string length!" << endl;
		return 1;
	}
}