

Credits:

Base game code by Jonathan Dearborn, placed in Public Domain where applicable.


2D character sprite
Art by Charles Gabriel. Commissioned by OpenGameArt.org (http://opengameart.org)


Background tiles from BrowserQuest
* Content is licensed under CC-BY-SA 3.0: 
http://creativecommons.org/licenses/by-sa/3.0/


Skeleton spritesheet based on LPC entry by
Johannes Sjölund
j.sjolund@gmail.com


UIPack from Kenney.nl


Music: Skye Cuillin - Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0
http://creativecommons.org/licenses/by/3.0/
