#include "Character.h"
#include "GameData.h"

void Character::Attack(GameData& gameData)
{
	// Visualization

	attackAnimationTimer = attackAnimationTimerMax;


	// Do damage to things

	int ex = x;
	int ey = y;
	switch (facing)
	{
	case 0:
		ey--;
		break;
	case 1:
		ex++;
		break;
	case 2:
		ey++;
		break;
	case 3:
		ex--;
		break;
	}
	Character* enemy = gameData.GetEnemy(ex, ey);
	if (enemy != nullptr)
	{
		gameData.DestroyEnemy(enemy);
		GPU_Log("Enemy destroyed!\n");
	}
	else
		GPU_Log("No enemy there...\n");
}