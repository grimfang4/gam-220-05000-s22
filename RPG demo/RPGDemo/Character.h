#pragma once

#include "SpriteSet.h"

class GameData;

// Character is the base class for moving/animating things on the play field
class Character
{
public:
	SpriteSet spriteSet;

	int cellSize;
	Vec2 offset;
	int x, y;

	int facing;
	bool isAnimating;
	float animationTimerMax;
	float animationTimer;

	// PingPong animation - Animate frames: 0,1,2,1,0,1,2 etc. instead of 0,1,2,0,1,2
	// Note: This class has an unfortunate hard-coded limit of 3 frames per animation
	enum class AnimationStateEnum {LOOP, ONCE, PING, PONG};
	AnimationStateEnum animationState;

	float attackAnimationTimer;
	float attackAnimationTimerMax;

	Character()
	{
		x = 0;
		y = 0;
		cellSize = 32;

		facing = 0;
		isAnimating = true;
		animationTimerMax = 0.25f;
		animationTimer = animationTimerMax;
		animationState = AnimationStateEnum::PING;

		attackAnimationTimer = -1;
		attackAnimationTimerMax = 0.25f;
	}

	virtual ~Character()
	{}

	virtual void Update(float dt)
	{
		attackAnimationTimer -= dt;
		if (attackAnimationTimer < 0)
		{
			attackAnimationTimer = -1;
			offset = Vec2(0,0);
		}
		else
		{
			float t = 1.0f - attackAnimationTimer / attackAnimationTimerMax;

			switch (facing)
			{
			case 0:
				offset.y = 10 * sin(t * M_PI);
				break;
			case 1:
				offset.x = 10 * sin(t * M_PI);
				break;
			case 2:
				offset.y = -10 * sin(t * M_PI);
				break;
			case 3:
				offset.x = -10 * sin(t * M_PI);
				break;
			}
		}


		if (!isAnimating)
			return;

		animationTimer -= dt;
		if (animationTimer <= 0.0f)
		{
			animationTimer = animationTimerMax;

			switch (animationState)
			{
			case AnimationStateEnum::LOOP:
				spriteSet.current.second++;
				if (spriteSet.current.second > 2)
					spriteSet.current.second = 0;
				break;
			case AnimationStateEnum::ONCE:
				if (spriteSet.current.second < 2)
					spriteSet.current.second++;
				else
					isAnimating = false;
				break;
			case AnimationStateEnum::PING:
				spriteSet.current.second++;
				if (spriteSet.current.second > 2)
				{
					spriteSet.current.second = 1;
					animationState = AnimationStateEnum::PONG;
				}
				break;
			case AnimationStateEnum::PONG:
				spriteSet.current.second--;
				if (spriteSet.current.second < 0)
				{
					spriteSet.current.second = 1;
					animationState = AnimationStateEnum::PING;
				}
				break;
			}
		}
	}

	virtual void SetFacing(int newFacing)
	{
		facing = newFacing;

		switch (facing)
		{
		case 0:
			spriteSet.current.first = "Up";
			break;
		case 1:
			spriteSet.current.first = "Right";
			break;
		case 2:
			spriteSet.current.first = "Down";
			break;
		case 3:
			spriteSet.current.first = "Left";
			break;
		}
	}

	virtual void Move(int dx, int dy)
	{
		bool doMove = false;
		if (dy < 0)
		{
			doMove = (facing == 2);
			SetFacing(2);
		}
		else if (dy > 0)
		{
			doMove = (facing == 0);
			SetFacing(0);
		}
		else if (dx > 0)
		{
			doMove = (facing == 1);
			SetFacing(1);
		}
		else if (dx < 0)
		{
			doMove = (facing == 3);
			SetFacing(3);
		}

		if (doMove)
		{
			x += dx;
			y += dy;
		}
	}

	virtual void Draw(GPU_Target* screen)
	{
		spriteSet.DrawCell(screen, cellSize, offset, x, y);
	}

	virtual void Attack(GameData& gameData);
};