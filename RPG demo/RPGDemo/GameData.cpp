#include "GameData.h"
#include <stdexcept>

// Helper used by GameData constructor
void LoadPlayerSprites(SpriteSet& spriteSet, GPU_Image* spriteSheet)
{
	// 16x18 sprites
	int spriteOffsetX = 4;  // 0 for warrior, 1 for magician, 2-healer, 3-ninja, 4-ranger, 5-townsfolk
	int spriteOffsetY = 1;  // 0 for male, 1 for female

	spriteOffsetX = 1 + 3 * spriteOffsetX * 16;
	spriteOffsetY = 1 + 4 * spriteOffsetY * 18;
	for (int i = 0; i < 3; ++i)
	{
		spriteSet.sprites.insert(make_pair(make_pair("Up", i), Sprite(spriteSheet, GPU_Rect{ (float)spriteOffsetX + i * 16, (float)spriteOffsetY + 36, 16.0f, 18.0f })));
		spriteSet.sprites.insert(make_pair(make_pair("Right", i), Sprite(spriteSheet, GPU_Rect{ (float)spriteOffsetX + i * 16, (float)spriteOffsetY + 18, 16.0f, 18.0f })));
		spriteSet.sprites.insert(make_pair(make_pair("Down", i), Sprite(spriteSheet, GPU_Rect{ (float)spriteOffsetX + i * 16, (float)spriteOffsetY, 16.0f, 18.0f })));
		spriteSet.sprites.insert(make_pair(make_pair("Left", i), Sprite(spriteSheet, GPU_Rect{ (float)spriteOffsetX + i * 16, (float)spriteOffsetY + 54, 16.0f, 18.0f })));
	}
	spriteSet.current = make_pair("Down", 0);
}

// Helper used by GameData constructor
void LoadSkeletonSprites(SpriteSet& spriteSet, GPU_Image* spriteSheet)
{
	// 32x32 sprites
	int spriteOffsetX;
	int spriteOffsetY;

	spriteOffsetX = 0;
	spriteOffsetY = 32 * 8;  // Get to the walk cycle sprites
	for (int i = 0; i < 3; ++i)
	{
		spriteSet.sprites.insert(make_pair(make_pair("Up", i), Sprite(spriteSheet, GPU_Rect{ (float)spriteOffsetX + i * 32, (float)spriteOffsetY, 32.0f, 32.0f })));
		spriteSet.sprites.insert(make_pair(make_pair("Left", i), Sprite(spriteSheet, GPU_Rect{ (float)spriteOffsetX + i * 32, (float)spriteOffsetY + 32, 32.0f, 32.0f })));
		spriteSet.sprites.insert(make_pair(make_pair("Down", i), Sprite(spriteSheet, GPU_Rect{ (float)spriteOffsetX + i * 32, (float)spriteOffsetY + 64, 32.0f, 32.0f })));
		spriteSet.sprites.insert(make_pair(make_pair("Right", i), Sprite(spriteSheet, GPU_Rect{ (float)spriteOffsetX + i * 32, (float)spriteOffsetY + 96, 32.0f, 32.0f })));
	}
	spriteSet.current = make_pair("Down", 0);
}



GameData::GameData()
{
	// Set up the screen
	screen = GPU_Init(800, 600, GPU_DEFAULT_INIT_FLAGS);

	if (screen == nullptr)
		throw std::runtime_error("Could not initialize SDL_gpu!");

	if (!MB::MixBox::instance().openMixer())
		GPU_LogError("Failed to create audio mixer.");

	// Load some music and sounds
	MB::MusicID music = MB::MixBox::instance().loadMusic("Assets/Music/Skye Cuillin.mp3");
	music.play();

	menuSound = MB::MixBox::instance().loadSound("Assets/Sounds/Menu1.wav");


	// Load the background tile positions from a text file, this file also refers to the tilesheet image file
	tileRenderer.Load("Assets/Levels/Level1.txt");
	

	// Load the player character into its own special reference (`player`)
	spriteSheet1 = images.Load("Assets/Images/Sprites/charsets_by_antifarea.png");

	player = new Character();
	
	LoadPlayerSprites(player->spriteSet, spriteSheet1);
	player->SetFacing(2);


	// Load the skeleton
	spriteSheet2 = images.Load("Assets/Images/Sprites/skeleton.png");

	Character* skeleton = new Character();
	LoadSkeletonSprites(skeleton->spriteSet, spriteSheet2);
	skeleton->SetFacing(2);

	enemies.push_back(skeleton);  // Add the skeleton to the list of enemies


	// Put some other object in the world
	SpriteObject* house = new SpriteObject();

	GPU_Image* spriteSheet3 = images.Load("Assets/Images/Tiles/tilesheet-browserquest.png");
	house->sprite = Sprite(spriteSheet3, GPU_Rect{96.0f, 320.0f, 32.0f*7, 32.0f*9});
	house->x = 15;
	house->y = 10;
	house->scale = 0.5f;
	objects.push_back(house);



	// Set up the persistent UI objects
	menuOpen = false;
	uiSpriteSheet1 = images.Load("Assets/Images/UI/yellowSheet.png");
	sansFont.load("Assets/Fonts/FreeSans.ttf", 16);
	uiFont.load("Assets/Fonts/kenvector_future.ttf", 16);

	quitButton.text = "Quit";
	quitButton.bounds = GPU_Rect{screen->w/2.0f - 50, screen->h/2 + 200.0f - 60, 100.0f, 50.0f};
	quitButton.font = &uiFont;
	quitButton.sprite.Set(uiSpriteSheet1, GPU_Rect{ 190.0f, 45.0f, 190.0f, 49.0f });




	// Other game loop details initialized
	done = false;

	dt = 0.0f;
	frameStartTime = 0;
	frameEndTime = 0;


	keystates = SDL_GetKeyboardState(nullptr);
}

GameData::~GameData()
{

	// Clean up all of the memory we allocated

	for (unsigned int i = 0; i < enemies.size(); ++i)
	{
		delete enemies[i];
	}
	enemies.clear();

	for (unsigned int i = 0; i < objects.size(); ++i)
	{
		delete objects[i];
	}
	objects.clear();

	delete player;

	images.Free();

	sansFont.free();
	uiFont.free();

	MB::MixBox::instance().freeSounds();
	MB::MixBox::instance().freeMusic();

	GPU_Quit();
}

void GameData::Restart()
{

}

void GameData::Run()
{
	// High-level main game loop

	dt = 0.0f;
	frameStartTime = SDL_GetTicks();

	done = false;

	Start();

	while (!done)
	{
		HandleEvents();
		HandleInputState();

		Update();
		Draw();

		EndFrame();
	}
}


Character* GameData::GetEnemy(int x, int y)
{
	for (int i = 0; i < enemies.size(); ++i)
	{
		if (enemies[i]->x == x && enemies[i]->y == y)
			return enemies[i];
	}
	return nullptr;
}

void GameData::DestroyEnemy(Character* enemy)
{
	auto e = std::find(enemies.begin(), enemies.end(), enemy);
	if (e != enemies.end())
	{
		delete *e;
		enemies.erase(e);
	}
}

void GameData::Start()
{

}

void GameData::HandleEvents()
{
	// Deal with incoming input events from the user (key presses, mouse clicks)
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
			done = true;
		else if (event.type == SDL_KEYDOWN)
		{
			switch (event.key.keysym.sym)
			{
				case SDLK_ESCAPE:
					done = true;
					break;
				case SDLK_RETURN:
					menuOpen = !menuOpen;
					menuSound.play();
					break;
				case SDLK_SPACE:
					if (!event.key.repeat)
						player->Attack(*this);
					break;
				case SDLK_w:
				case SDLK_UP:
					if(!event.key.repeat)
						player->Move(0, -1);
					break;
				case SDLK_DOWN:
				case SDLK_s:
					if (!event.key.repeat)
						player->Move(0, 1);
					break;
				case SDLK_LEFT:
				case SDLK_a:
					if (!event.key.repeat)
						player->Move(-1, 0);
					break;
				case SDLK_RIGHT:
				case SDLK_d:
					if (!event.key.repeat)
						player->Move(1, 0);
					break;
			}
		}
		else if (event.type == SDL_MOUSEBUTTONDOWN)
		{
			if (menuOpen)
			{
				if (quitButton.InBounds(event.button.x, event.button.y))
				{
					done = true;
				}
			}
		}
	}
}

void GameData::HandleInputState()
{
	// Continuous player input: Moving smoothly or holding keys down
	/*if (keystates[SDL_SCANCODE_W])
		player->velocity.y = -100;
	else if (keystates[SDL_SCANCODE_S])
		player->velocity.y = 100;
	else
		player->velocity.y = 0;

	if (keystates[SDL_SCANCODE_A])
		player->velocity.x = -100;
	else if (keystates[SDL_SCANCODE_D])
		player->velocity.x = 0;
	else
		player->velocity.x = 0;*/
}

void GameData::Update()
{
	// Give all of the objects in the world a chance to do their own logic each frame

	player->Update(dt);

	for (unsigned int i = 0; i < enemies.size(); ++i)
	{
		enemies[i]->Update(dt);
	}

	for (unsigned int i = 0; i < objects.size(); ++i)
	{
		objects[i]->Update(dt);
	}
}

void GameData::Draw()
{
	// Draw the world

	// Background color
	GPU_ClearRGBA(screen, 150, 200, 150, 255);

	// Background tiles
	tileRenderer.Draw(screen);


	// World objects in back to front order

	for (unsigned int i = 0; i < objects.size(); ++i)
	{
		objects[i]->Draw(screen);
	}

	for (unsigned int i = 0; i < enemies.size(); ++i)
	{
		enemies[i]->Draw(screen);
	}

	player->Draw(screen);


	// UI

	// Right panel
	GPU_RectangleRoundFilled(screen, screen->w - 160, 0, screen->w, screen->h - 80, 10, GPU_MakeColor(0, 0, 0, 180));
	sansFont.draw(screen, screen->w - 160 + 5, 0, NFont::Effect(NFont::AlignEnum::LEFT, NFont::Color(255, 255, 255)), "Stuff");
	sansFont.draw(screen, screen->w - 5, 40, NFont::Effect(NFont::AlignEnum::RIGHT, NFont::Color(255, 255, 255)), "Some Number: %.1f", 3.14f);
	sansFont.draw(screen, screen->w - 5, 60, NFont::Effect(NFont::AlignEnum::RIGHT, NFont::Color(255, 255, 255)), "Some Integer: %d", 5);

	// Bottom panel
	GPU_RectangleFilled(screen, 0, screen->h - 80, screen->w, screen->h, GPU_MakeColor(20, 20, 100, 255));
	sansFont.draw(screen, 5, screen->h - 80, NFont::Effect(NFont::AlignEnum::LEFT, NFont::Color(255, 255, 255)), "Stuff here too");

	if (menuOpen)
	{
		// Menu (opens with spacebar)
		GPU_Rect menuPanel = {screen->w/2 - 200, screen->h/2 - 200, 400, 400};
		GPU_RectangleRoundFilled2(screen, menuPanel, 10, GPU_MakeColor(100, 100, 100, 220));
		GPU_RectangleRound2(screen, menuPanel, 10, GPU_MakeColor(0, 0, 0, 255));
		quitButton.Draw(screen);
	}

}

void GameData::EndFrame()
{
	GPU_Flip(screen);  // Show the screen to the user
	// Frame is done rendering
	SDL_Delay(1);  // Give some time back to the OS

	// Update our frame timing
	frameEndTime = SDL_GetTicks();
	dt = (frameEndTime - frameStartTime) / 1000.0f;
	frameStartTime = frameEndTime;
}