#pragma once

#include <string>
#include <vector>
#include "SDL_gpu.h"
#include "ImageManager.h"
#include "NFont_gpu.h"
#include "Random.h"
#include "TileRenderer.h"
#include "Character.h"
#include "SpriteObject.h"
#include "Button.h"
#include "MixBox.h"

using namespace std;

class GameData
{
public:
	GPU_Target* screen;

	ImageManager images;
	NFont sansFont;
	NFont uiFont;
	Random rnd;

	TileRenderer tileRenderer;

	GPU_Image* spriteSheet1;
	GPU_Image* spriteSheet2;
	GPU_Image* uiSpriteSheet1;

	Character* player;
	vector<Character*> enemies;
	vector<SpriteObject*> objects;

	bool menuOpen;
	Button quitButton;

	MB::SoundID menuSound;


	float dt;
	bool done;


	GameData();
	~GameData();

	void Restart();
	void Run();

	Character* GetEnemy(int x, int y);
	void DestroyEnemy(Character* enemy);

private:

	const Uint8* keystates;

	Uint32 frameStartTime;
	Uint32 frameEndTime;

	void Start();

	void HandleEvents();
	void HandleInputState();

	void Update();
	void Draw();
	void EndFrame();

};