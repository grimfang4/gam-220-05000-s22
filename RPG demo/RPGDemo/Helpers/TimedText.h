#pragma once

#include "SDL_gpu.h"
#include "NFont_gpu.h"
#include <string>
using namespace std;

class TimedText
{
public:
	float x, y;
	float lifetime;
	float maxLifetime;
	NFont* font;
	string text;
	SDL_Color color;

	TimedText(float newX, float newY, float newLifetime, NFont* newFont, string newText);

	void Update(float dt);

	void Draw(GPU_Target* target);
};