#pragma once


#include "SDL_gpu.h"
#include "Vec2.h"

// Sprite is responsible for referencing a section of an image and rendering to the screen
class Sprite
{
public:
	GPU_Image* spritesheet;
	GPU_Rect sourceRect;

	Sprite();
	Sprite(GPU_Image* sheet, GPU_Rect sourceRect);

	void Set(GPU_Image* sheet, GPU_Rect sourceRect);

	void Draw(GPU_Target* screen, float x, float y) const;
	void DrawCell(GPU_Target* screen, int cellSize, const Vec2& offset, int x, int y) const;
	void DrawCellScale(GPU_Target* screen, int cellSize, const Vec2& offset, int x, int y, float scale) const;
	void DrawBounds(GPU_Target* screen, const GPU_Rect& bounds) const;
};